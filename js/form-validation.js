function beans() {
  var carrier_num = parseInt(document.getElementById("carrier_num").value,10);
  var fighter_num = parseInt(document.getElementById("fighter_num").value,10);
  var destroyer_num = parseInt(document.getElementById("destroyer_num").value,10);
  var cruiser_num = parseInt(document.getElementById("cruiser_num").value,10);
  var dreadnought_num = parseInt(document.getElementById("dreadnought_num").value,10);
  var flagship_num = parseInt(document.getElementById("flagship_num").value,10);
  var war_sun_num = parseInt(document.getElementById("war_sun_num").value,10);

  var carrier_hit = parseInt(document.getElementById("carrier_hit").value,10);
  var fighter_hit = parseInt(document.getElementById("fighter_hit").value,10);
  var destroyer_hit = parseInt(document.getElementById("destroyer_hit").value,10);
  var cruiser_hit = parseInt(document.getElementById("cruiser_hit").value,10);
  var dreadnought_hit = parseInt(document.getElementById("dreadnought_hit").value,10);
  var flagship_hit = parseInt(document.getElementById("flagship_hit").value,10);
  var war_sun_hit = parseInt(document.getElementById("war_sun_hit").value,10);

  var input_string = "This was calculated using the following numbers:"
  + "\ncarrier_num:            " + carrier_num + "   |   carrier_hit :            " + carrier_hit
  + "\nfighter_num:            " + fighter_num + "   |   fighter_hit:            " + fighter_hit 
  + "\ndestroyer_num:       " + destroyer_num + "   |   destroyer_hit:        " + destroyer_hit 
  + "\ncruiser_num:            " + cruiser_num + "   |   cruiser_hit:            " + cruiser_hit 
  + "\ndreadnought_num:  " + dreadnought_num + "   |   dreadnought_hit:  " + dreadnought_hit 
  + "\nflagship_num:          " + flagship_num + "   |   flagship_hit:          " + flagship_hit 
  + "\nwar_sun_num:          " + war_sun_num + "   |   war_sun_hit:          " + war_sun_hit;
  
  var error_string = "\n\n\n----- WARNING, below needs review -----";
  //carriers
  if(isNaN(carrier_hit) && (!isNaN(carrier_num))){
	  error_string += "\n - minimum carrier hit is invalid while number of carrier rolls are valid";
  }
  if(isNaN(carrier_num) && (!isNaN(carrier_hit))){
	error_string += "\n - number of carrier rolls are invalid while minimum carrier hit is valid";
  }
  //fighters
  if(isNaN(fighter_hit) && (!isNaN(fighter_num))){
	  error_string += "\n - minimum fighter hit is invalid while number of fighter rolls are valid";
  }
  if(isNaN(fighter_num) && (!isNaN(fighter_hit))){
	error_string += "\n - number of fighter rolls are invalid while minimum fighter hit is valid";
  }
  //destroyers
  if(isNaN(destroyer_hit) && (!isNaN(destroyer_num))){
	  error_string += "\n - minimum destroyer hit is invalid while number of destroyer rolls are valid";
  }
  if(isNaN(destroyer_num) && (!isNaN(destroyer_hit))){
	error_string += "\n - number of destroyer rolls are invalid while minimum destroyer hit is valid";
  }
  //cruiser
  if(isNaN(cruiser_hit) && (!isNaN(cruiser_num))){
	  error_string += "\n - minimum cruiser hit is invalid while number of cruiser rolls are valid";
  }
  if(isNaN(cruiser_num) && (!isNaN(cruiser_hit))){
	error_string += "\n - number of cruiser rolls are invalid while minimum cruiser hit is valid";
  }
  //dreadnought
  if(isNaN(dreadnought_hit) && (!isNaN(dreadnought_num))){
	  error_string += "\n - minimum dreadnought hit is invalid while number of dreadnought rolls are valid";
  }
  if(isNaN(dreadnought_num) && (!isNaN(dreadnought_hit))){
	error_string += "\n - number of dreadnought rolls are invalid while minimum dreadnought hit is valid";
  }
  //flagship
  if(isNaN(flagship_hit) && (!isNaN(flagship_num))){
	  error_string += "\n - minimum flagship hit is invalid while number of flagship rolls are valid";
  }
  if(isNaN(flagship_num) && (!isNaN(flagship_hit))){
	error_string += "\n - number of flagship rolls are invalid while minimum flagship hit is valid";
  }
  //war_sun
  if(isNaN(war_sun_hit) && (!isNaN(war_sun_num))){
	  error_string += "\n - minimum war_sun hit is invalid while number of war sun rolls are valid";
  }
  if(isNaN(war_sun_num) && (!isNaN(war_sun_hit))){
	error_string += "\n - number of war sun rolls are invalid while minimum war sun hit is valid";
  }
  
  if(error_string!="\n\n\n----- WARNING, below needs review -----"){
    input_string += error_string;
  }
  
    // convert NaN to 0 
    //carrier
	if(isNaN(carrier_num)){
		carrier_num = 0;
	}
	if(isNaN(carrier_hit)){
		carrier_hit = 0;
	}
	//fighter
	if(isNaN(fighter_num)){
		fighter_num = 0;
	}
	if(isNaN(fighter_hit)){
		fighter_hit = 0;
	}
	//destroyer 
	if(isNaN(destroyer_num)){
		destroyer_num = 0;
	}
	if(isNaN(destroyer_hit)){
		destroyer_hit = 0;
	}
	//cruiser
	if(isNaN(cruiser_num)){
		cruiser_num = 0;
	}
	if(isNaN(cruiser_hit)){
		cruiser_hit = 0;
	}
	//dreadnought
	if(isNaN(dreadnought_num)){
		dreadnought_num = 0;
	}
	if(isNaN(dreadnought_hit)){
		dreadnought_hit = 0;
	}
	//flagship
	if(isNaN(flagship_num)){
		flagship_num = 0;
	}
	if(isNaN(flagship_hit)){
		flagship_hit = 0;
	}
	//war sun
	if(isNaN(war_sun_num)){
		war_sun_num = 0;
	}
	if(isNaN(war_sun_hit)){
		war_sun_hit = 0;
	}
	// looping logic
	hits = 0;
	var i;
	var d10 = 0;
	
	var carrier_hits = 0;
	var fighter_hits = 0;
	var destroyer_hits = 0;
	var cruiser_hits = 0;
	var dreadnought_hits = 0;
	var flagship_hits = 0;
	var war_sun_hits = 0;
	
	//carrier
	for (i = 0; i < carrier_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= carrier_hit){
			hits += 1;
			carrier_hits += 1;
		}
	}
	//fighter
	for (i = 0; i < fighter_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= fighter_hit){
			hits += 1;
			fighter_hits += 1;
		}
	}
	//destroyer
	for (i = 0; i < destroyer_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= destroyer_hit){
			hits += 1;
			destroyer_hits += 1;
		}
	}
	//cruiser
	for (i = 0; i < cruiser_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= cruiser_hit){
			hits += 1;
			cruiser_hits += 1;
		}
	}
	//dreadnought
	for (i = 0; i < dreadnought_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= dreadnought_hit){
			hits += 1;
			dreadnought_hits += 1;
		}
	}
	//flagship
	for (i = 0; i < flagship_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= flagship_hit){
			hits += 1;
			flagship_hits += 1;
		}
	}
	//war sun
	for (i = 0; i < war_sun_num; i++) {
		d10 = Math.floor(Math.random() * 10) + 1;
		if(d10 >= war_sun_hit){
			hits += 1;
			war_sun_hits += 1;
		}
	}
	
	var hit_string = "\n-----Hit tabulation-----"
	+ "\ncarrier_num:            " + carrier_num + "   |   carrier_hits:            " + carrier_hits
	+ "\nfighter_num:            " + fighter_num + "   |   fighter_hits:            " + fighter_hits
	+ "\ndestroyer_num:       " + destroyer_num + "   |   destroyer_hits:        " + destroyer_hits
	+ "\ncruiser_num:            " + cruiser_num + "   |   cruiser_hits:            " + cruiser_hits
	+ "\ndreadnought_num:  " + dreadnought_num + "   |   dreadnought_hits:  " + dreadnought_hits
	+ "\nflagship_num:          " + flagship_num + "   |   flagship_hits:          " + flagship_hits
	+ "\nwar_sun_num:          " + war_sun_num + "   |   war_sun_hits:          " + war_sun_hits;
 
	input_string += hit_string;
	var ans_string = "total_hits:    " + hits + "\n\n";
	input_string = ans_string + input_string;
  alert(input_string);
}

